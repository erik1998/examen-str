public class ExecutionThread1 extends Thread {
    Integer monitor1;
    int sleep_min, sleep_max, activity_min, activity_max;

    public ExecutionThread(Integer monitor1, int sleep_min, int sleep_max, int activity_min, int activity_max) {
        this.monitor1 = monitor1;
        this.sleep_min = sleep_min;
        this.sleep_max = sleep_max;
        this.activity_min = activity_min;
        this.activity_max = activity_max;
    }

    public void run() {
        System.out.println(this.getName() + " - STATE 1");
        try {
            Thread.sleep(Math.round(Math.random() * (sleep_max - sleep_min)+ sleep_min) * 500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(this.getName() + " - STATE 2");
        synchronized (monitor1) {
            a1=true;
            System.out.println(this.getName() + " - STATE 3");
            int k = (int) Math.round(Math.random()*(activity_max - activity_min) + activity_min);
            for (int i = 0; i < k * 100000; i++) {
                i++; i--;
            }
            a1=false;
            monitor1.wait.notify();
        }
        System.out.println(this.getName() + " - STATE 4");
    }
}

public class ExecutionThread2 extends Thread {
    Integer monitor1;
    Integer monitor2;
    int sleep_min, sleep_max, activity_min, activity_max;

    public ExecutionThread(Integer monitor1, Integer monitor2, int sleep_min, int 	sleep_max, int activity_min, int activity_max) {
        this.monitor1 = monitor1;
        this.monitor2 = monitor2;
        this.sleep_min = sleep_min;
        this.sleep_max = sleep_max;
        this.activity_min = activity_min;
        this.activity_max = activity_max;
    }

    public void run() {
        System.out.println(this.getName() + " - STATE 1");
        try {
            Thread.sleep(Math.round(Math.random() * (sleep_max - sleep_min)+ sleep_min) * 500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(this.getName() + " - STATE 2");
        if (a1==false)
        synchronized (monitor1) {
            System.out.println(this.getName() + " - STATE 3");
            int k = (int) Math.round(Math.random()*(activity_max - activity_min) + activity_min);
            for (int i = 0; i < k * 100000; i++) {
                i++; i--;
            }

        }
        else
            monitor1.wait();
        if (a2==false)
        synchronized (monitor2) {
            System.out.println(this.getName() + " - STATE 3");
            int k = (int) Math.round(Math.random()*(activity_max - activity_min) + activity_min);
            for (int i = 0; i < k * 100000; i++) {
                i++; i--;
            }

        }
        else monitor2.wait();
        System.out.println(this.getName() + " - STATE 4");
    }
}

public class ExecutionThread3 extends Thread {
    Integer monitor2;
    int sleep_min, sleep_max, activity_min, activity_max;

    public ExecutionThread(Integer monitor2, int sleep_min, int sleep_max, int activity_min, int activity_max) {
        this.monitor2 = monitor2;
        this.sleep_min = sleep_min;
        this.sleep_max = sleep_max;
        this.activity_min = activity_min;
        this.activity_max = activity_max;
    }

    public void run() {
        System.out.println(this.getName() + " - STATE 1");
        try {
            Thread.sleep(Math.round(Math.random() * (sleep_max - sleep_min)+ sleep_min) * 500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(this.getName() + " - STATE 2");
        synchronized (monitor2) {
            a2=true;
            System.out.println(this.getName() + " - STATE 3");
            int k = (int) Math.round(Math.random()*(activity_max - activity_min) + activity_min);
            for (int i = 0; i < k * 100000; i++) {
                i++; i--;
            }
            a2=false;
            monitor2.wait.notify();
        }
        System.out.println(this.getName() + " - STATE 4");
    }
}

public class Main {
    public static void main(String[] args) {
        boolean a1=false;
        boolean a2=false;
        Integer monitor1 = new Integer(1);
        Integer monitor2 = new Integer(1);
        new ExecutionThread1(monitor1, 2, 4, 3, 6).start();
        new ExecutionThread2(monitor1, monitor2, 3, 5, 4, 7).start();
        new ExecutionThread3(monitor2, 3, 5, 4, 7).start();
    }
}
